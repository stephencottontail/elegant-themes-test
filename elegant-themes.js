/**
 * Elegant Themes developer test JS functions
 */

jQuery( document ).ready( function( $ ) {
	var form = $( '.skdet-form' );
	var button = $( '.skdet-submit' );
	var name = $( '#skdet-name' );
	var email = $( '#skdet-email' );
	var phone = $( '#skdet-phone' );
	var budget = $( '#skdet-budget' );
	var notes = $( '#skdet-notes' );
	var result = form.find( '#result' );
	
	button.on( 'click', function( e ) {
		e.preventDefault();
		
		$form = $(this);
		var today = new Date();
		var date = today.getFullYear() + "-" + ( today.getMonth() + 1 ) + "-" + today.getDate();
		var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
		var dateTime = date + ' ' + time;
		
		console.log( dateTime );
		var data = {
			'action': 'generate_lead',
			'name': name.val(),
			'email': email.val(),
			'phone': phone.val(),
			'budget': budget.val(),
			'notes': notes.val(),
			'date': dateTime
		};
		
		$.ajax( {
			type: "post",
			url: ajax_object.ajax_url,
			data: data,
			success: function( response ) {
				if ( -1 == response ) {
					result.html( '<p class="error">You are not logged in!</p>' );
				} else {
					result.html( '<p class="success">Success!</p>' )
				}
			}
		} );
	} );
} );