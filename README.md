# Elegant Themes Developer Test

I created this plugin as part of a developer test for Elegant Themes.

## Notes

* To be consistent with your chosen theme's design, the front end elements are light on CSS
* Currently, the responses from AJAX are not translatable
* Currently, client-side validating is not implemented