<?php
/*
Plugin Name: Elegant Themes Test
Author: Stephen Dickinson
Version: 1.0
Description: I created this plugin as part of a developer test for Elegant Themes.
Text Domain: skdet
*/

function lead_forms_gen( $atts ) {
	$skd_atts = shortcode_atts( array(
		'name'      => esc_html__( 'Name', 'skdet' ),
		'phone'     => esc_html__( 'Phone Number', 'skdet' ),
		'email'     => esc_html__( 'Email', 'skdet' ),
		'budget'    => esc_html__( 'Desired Budget', 'skdet' ),
		'notes'     => esc_html__( 'Notes', 'skdet' ),
		'name_max'  => null,
		'rows'      => 8,
		'cols'      => 40
	), $atts );
	
	/* testing */
	/* end testing */

	/* build form */
	?>
	<form class="skdet-form">
		<p class="skdet-form-element skdet-form-name">
			<label for="skdet-name"><?php echo esc_html( $skd_atts['name'] ); ?></label>
			<input type="text" id="skdet-name" maxlength="<?php echo ( $skd_atts['name_max'] ) ? esc_attr( $skd_atts['name_max'] ) : null ?>" />
		</p>
		<p class="skdet-form-element skdet-form-phone">
			<label for="skdet-phone"><?php echo esc_html( $skd_atts['phone'] ); ?></label>
			<input type="text" id="skdet-phone" />
		</p>
		<p class="skdet-form-element skdet-form-email">
			<label for="skdet-email"><?php echo esc_html( $skd_atts['email'] ); ?></label>
			<input type="email" id="skdet-email" />
		</p>
		<p class="skdet-form-element skdet-form-budget">
			<label for="skdet-budget"><?php echo esc_html( $skd_atts['budget'] ); ?></label>
			<input type="text" id="skdet-budget" />
		</p>
		<p class="skdet-form-element skdet-form-notes">
			<label for="skdet-notes"><?php echo esc_html( $skd_atts['notes'] ); ?></label>
			<textarea id="skdet-notes" rows="<?php echo esc_attr( $skd_atts['rows'] ); ?>" cols="<?php echo esc_attr( $skd_atts['cols'] ); ?>"></textarea>
		</p>
		
		<input type="submit" class="skdet-submit" value="<?php echo esc_attr( __( 'Submit', 'skdet' ) ); ?>" />
		<input type="hidden" /><!-- need to get date and time somehow, thru PHP? -->
		<div id="result"></div>
	</form>
<?php
}
add_shortcode( 'lead_form', 'lead_forms_gen' );

function skdet_create_customer_post_type() {
	register_post_type( 'skdet_customer', array(
		'labels'      => array(
			'name'          => __( 'Customers', 'skdet' ),
			'singular_name' => __( 'Customer', 'skdet' ),
			'all_items'     => __( 'All Customers', 'skdet' )
			),
		'public'      => true,
		'show_ui'     => true,
		'has_archive' => true,
		'rewrite'     => array( 'slug' => 'customers' )
	) );
}
add_action( 'init', 'skdet_create_customer_post_type' );

function load_scripts() {
	wp_enqueue_style( 'skdet-style', plugins_url( 'elegant-themes.css', __FILE__ ) );
	
	wp_enqueue_script( 'skdet-script', plugins_url( 'elegant-themes.js', __FILE__ ), array( 'jquery' ) );
	wp_localize_script( 'skdet-script', 'ajax_object', array( 'ajax_url' => esc_url( admin_url( 'admin-ajax.php' ) ) ) );
}
add_action( 'wp_enqueue_scripts', 'load_scripts', 20 );

function skdet_generate_lead() {
	global $wpdb;
	
	$not_provided = sprintf( '<span class="not-provided">%s</span>', esc_html__( '(not provided)', 'skdet' ) );
	$customer_post_content = '<div class="skdet-customer-wrapper">' .
		sprintf( '<p class="skdet-customer-data skdet-customer-phone"><strong>%1$s</strong>%2$s</p>', esc_html__( 'Phone: ', 'skdet' ), ( $_POST['phone'] ) ? $_POST['phone'] : $not_provided ) .
		sprintf( '<p class="skdet-customer-data skdet-customer-email"><strong>%1$s</strong>%2$s</p>', esc_html__( 'Email: ', 'skdet' ), ( $_POST['email'] ) ? $_POST['email'] : $not_provided ) .
		sprintf( '<p class="skdet-customer-data skdet-customer-budget"><strong>%1$s</strong>%2$s</p>', esc_html__( 'Budget: ', 'skdet' ), ( $_POST['budget'] ) ? $_POST['budget'] : $not_provided ) .
		sprintf( '<p class="skdet-customer-data skdet-customer-date-added"><strong>%1$s</strong>%2$s</p>', esc_html__( 'Date Added: ', 'skdet' ), $_POST['date'] ).
		'</div>' . $_POST['notes'];

	$new_customer = array(
		'post_type'    => 'skdet_customer',
		'post_title'   => wp_strip_all_tags( $_POST['name'] ),
		'post_content' => $customer_post_content,
		'post_status'  => 'publish',
		'meta_input'   => array(
			'phone'  => $_POST['phone'],
			'email'  => $_POST['email'],
			'budget' => $_POST['budget'],
			'date'   => $_POST['date']
		)
	);
	
	wp_insert_post( $new_customer );

	wp_die( 1 );
}
add_action( 'wp_ajax_generate_lead', 'skdet_generate_lead' );

function skdet_not_privileged() {
	wp_die( -1 );
}
add_action( 'wp_ajax_nopriv_generate_lead', 'skdet_not_privileged' );